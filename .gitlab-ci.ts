import { Config, CreateConfigFunction } from "gitlab-run";

const createConfig: CreateConfigFunction = async () => {
    const config = new Config();

    config.stages("build", "test", "default");

    config.defaults({
        image: "alpine:latest",
    });

    // Setting variables globally or per job
    config.variable("DOCKER_DRIVER", "overlay2");

    // Run a job only in production branch
    config.job(
        "only production",
        {
            only: {
                refs: ["master"],
            },
        },
        true // Creates a hidden job (prefixed with a dot)
    );

    // Allows you to include further configurations by glob patterns
    await config.include(__dirname, ["devops/.gitlab/*.ts"]);
    await config.include(__dirname, ["packages/*/devops/.gitlab/.gitlab-ci.ts"]);

    return config;
};

export { createConfig };

function __dirname(__dirname: any, arg1: string[]) {
    throw new Error("Function not implemented.");
}
