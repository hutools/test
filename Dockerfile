FROM node:lts-alpine

# Install dependencies
RUN apk add --no-cache git

# This is the easist way currently, just install from registry
RUN npm install -g gitlab-run@1.0.2

# Make gitlab-run module available globally
ENV NODE_PATH=/usr/local/lib/node_modules